#/usr/bin/env python

# pyelf imports
from elftools import __version__
from elftools.common.exceptions import ELFError
from elftools.common.py3compat import (
        ifilter, byte2int, bytes2str, itervalues, str2bytes)
from elftools.elf.elffile import ELFFile
from elftools.elf.dynamic import DynamicSection, DynamicSegment
from elftools.elf.enums import ENUM_D_TAG
from elftools.elf.segments import InterpSegment
from elftools.elf.sections import SymbolTableSection
from elftools.elf.relocation import RelocationSection
from elftools.elf.descriptions import (
    describe_ei_class, describe_ei_data, describe_ei_version,
    describe_ei_osabi, describe_e_type, describe_e_machine,
    describe_e_version_numeric, describe_p_type, describe_p_flags,
    describe_sh_type, describe_sh_flags,
    describe_symbol_type, describe_symbol_bind, describe_symbol_visibility,
    describe_symbol_shndx, describe_reloc_type, describe_dyn_tag,
    )
from elftools.dwarf.dwarfinfo import DWARFInfo
from elftools.dwarf.descriptions import (
    describe_reg_name, describe_attr_value, set_global_machine_arch,
    describe_CFI_instructions, describe_CFI_register_rule,
    describe_CFI_CFA_rule,
    )
from elftools.dwarf.constants import (
    DW_LNS_copy, DW_LNS_set_file, DW_LNE_define_file)
from elftools.dwarf.callframe import CIE, FDE
from elftools.dwarf.dwarfinfo import DWARFInfo, DebugSectionDescriptor, DwarfConfig

# normal imports
import os
import sys
import re

# checklist
# =========
#
# 1. check if files are ELF files.
# 2. match ELF machine
# 3. match ELF CLASS (ELF32 / ELF64).
# 4. match OSABI match.
# 5. match ELF type / kind (ET_DYN / ET_EXEC) == elf_kind()
# 6. match EI_VERSION
# 7. match the SONAMEs
# 8. collect symbol information from the dynamic symbol table, walk symbol
#    table and find differences // XXX

class ELF(object):

    def __init__(self):
        self._dwarffile = None
        self._dwarfinfo = None

    def is_elf(self, fh):
        try:
            self.fh = fh
            self.elffile = ELFFile(fh)
            self.elffile = ELFFile(fh)
            self.header = self.elffile.header
            self.e_ident = self.elffile.header['e_ident']
        except ELFError:
            return False
        return True

    def elf_machine(self):
        """elf_machine ==> Intel 80386 / Advanced Micro Devices X86-64
        determination"""

        self.machine = describe_e_machine(self.header['e_machine'])
        print self.machine

    def elf_class(self):
        """elf_class ==> ELF32 / ELF64 determination"""

        self.elfclass = describe_ei_class(self.e_ident['EI_CLASS'])
        print self.elfclass

    def elf_osabi(self):
        """elf_osabi ==> should be ELFOSABI_LINUX == UNIX - Linux *or*
        ELFOSABI_SYSV='UNIX - System V'"""

        self.osabi = describe_ei_osabi(self.e_ident['EI_OSABI'])
        print self.osabi

    def elf_kind(self):
        """elf_kind ==> ET_DYN / ET_EXEC determination"""

        self.kind = describe_e_type(self.header['e_type'])
        print self.kind

    def elf_eiversion(self):
        self.eiversion = describe_ei_version(self.e_ident['EI_VERSION'])
        print self.eiversion

    def elf_version(self):
        self.version = describe_e_version_numeric(self.header['e_version'])
        print self.version

    def elf_soname(self):
        """Dynamic Section, If an object file participates in dynamic linking,
        its program header table will have an element of type PT_DYNAMIC. This
        "segment" contains the .dynamic section. A special symbol, _DYNAMIC,
        labels the section, which contains an array of the following
        structures.

        Program Header has p_type member which tells what kind of segment this
        array element describes or how to interpret the array element's
        information

        If p_type is PT_DYNAMIC then it specifies dynamic linking information."""

        if self.elffile.num_segments() == 0:
            print('There are no program headers in this file!')
            return

        for segment in self.elffile.iter_segments():
            print segment['p_type']
            if re.search("PT_DYNAMIC", segment['p_type']):
                # this file uses dynamic linking, so read the dynamic section
                # and find DT_SONAME tag
                for section in self.elffile.iter_sections():
                    if not isinstance(section, DynamicSection):
                        continue
                    for tag in section.iter_tags():
                        if tag.entry.d_tag == 'DT_SONAME':
                            self.soname = bytes2str(tag.soname)
                            print self.soname
                            return
                break

        self.soname = None

    def elf_symbols(self):
        """parse dynamic symbol table"""

        pass


    def display_symbol_tables(self):
        """ Display the symbol tables contained in the file
        """
        for section in self.elffile.iter_sections():
            if not isinstance(section, SymbolTableSection):
                continue

            if section['sh_entsize'] == 0:
                print("\nSymbol table '%s' has a sh_entsize of zero!" % (
                    bytes2str(section.name)))
                continue

            print("\nSymbol table '%s' contains %s entries:" % (
                bytes2str(section.name), section.num_symbols()))

            if self.elffile.elfclass == 32:
                print('   Num:    Value  Size Type    Bind   Vis      Ndx Name')
            else: # 64
                print('   Num:    Value          Size Type    Bind   Vis      Ndx Name')

            for nsym, symbol in enumerate(section.iter_symbols()):
                # symbol names are truncated to 25 chars, similarly to readelf
                print('%6d: %s %5d %-7s %-6s %-7s %4s %.25s' % (
                    nsym,
                    symbol['st_value'],
                    symbol['st_size'],
                    describe_symbol_type(symbol['st_info']['type']),
                    describe_symbol_bind(symbol['st_info']['bind']),
                    describe_symbol_visibility(symbol['st_other']['visibility']),
                    describe_symbol_shndx(symbol['st_shndx']),
                    bytes2str(symbol.name)))


    def get_dwarf_info(self, relocate_dwarf_sections=True):
        """ Return a DWARFInfo object representing the debugging information in
            this file.

            If relocate_dwarf_sections is True, relocations for DWARF sections
            are looked up and applied.
        """
        # Expect that has_dwarf_info was called, so at least .debug_info is
        # present.
        # Sections that aren't found will be passed as None to DWARFInfo.
        #
        debug_sections = {}
        for secname in (b'.debug_info', b'.debug_abbrev', b'.debug_str',
                        b'.debug_line', b'.debug_frame', b'.debug_loc',
                        b'.debug_ranges'):
            section = self.get_section_by_name(secname)
            if section is None:
                debug_sections[secname] = None
            else:
                debug_sections[secname] = self._read_dwarf_section(
                        section,
                        relocate_dwarf_sections)

        return DWARFInfo(
                config=DwarfConfig(
                    little_endian=self.little_endian,
                    default_address_size=self.elfclass / 8,
                    machine_arch=self.get_machine_arch()),
                debug_info_sec=debug_sections[b'.debug_info'],
                debug_abbrev_sec=debug_sections[b'.debug_abbrev'],
                debug_frame_sec=debug_sections[b'.debug_frame'],
                debug_str_sec=debug_sections[b'.debug_str'],
                debug_loc_sec=debug_sections[b'.debug_loc'],
                debug_ranges_sec=debug_sections[b'.debug_ranges'],
                debug_line_sec=debug_sections[b'.debug_line'])

    def _init_dwarfinfo(self, filename=None):
        """ Initialize the DWARF info contained in the file and assign it to
            self._dwarfinfo.
            Leave self._dwarfinfo at None if no DWARF info was found in the file
        """
        if self._dwarfinfo is not None:
            return

        if self.elffile.has_dwarf_info():
            self._dwarfinfo = self.elffile.get_dwarf_info()
        else:
            self._dwarfinfo = None
            if not filename:
                print self.fh.name, "doesn't have DWARF info in it!"
                print ":-("
                return
            else:
                # create ELFFile object
                delf = ELFFile(open(filename, "rb"))
                print delf.has_dwarf_info()
                self._dwarfinfo = delf.get_dwarf_info()
                print ":-) patched"

        for CU in self._dwarfinfo.iter_CUs():
            # DWARFInfo allows to iterate over the compile units contained in
            # the .debug_info section. CU is a CompileUnit object, with some
            # computed attributes (such as its offset in the section) and
            # a header which conforms to the DWARF standard. The access to
            # header elements is, as usual, via item-lookup.
            print('  Found a compile unit at offset %s, length %s' % (
                CU.cu_offset, CU['unit_length']))

            # The first DIE in each compile unit describes it.
            top_DIE = CU.get_top_DIE()
            print('    Top DIE with tag=%s' % top_DIE.tag)


    def display_debug_dump(self, filename=None):

        """ Dump a DWARF section
        """
        self._init_dwarfinfo(filename)
        if self._dwarfinfo is None:
            return

        set_global_machine_arch(self.elffile.get_machine_arch())


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: %s <ELF file> [debuginfo file]" % sys.argv[0])
        print("\ne.g python abidiff.py examples/openssl examples/openssl.debug");
        print("\ne.g python abidiff.py examples/main");
        sys.exit(-1)

    fh = open(sys.argv[1])
    abidiff = ELF()
    print abidiff.is_elf(fh)
    abidiff.elf_kind()
    abidiff.elf_class()
    abidiff.elf_machine()
    abidiff.elf_osabi()
    abidiff.elf_eiversion()
    abidiff.elf_version()
    abidiff.elf_soname()
    # abidiff.display_symbol_tables()
    if len(sys.argv) < 3:
        abidiff.display_debug_dump(None)
    else:
        abidiff.display_debug_dump(sys.argv[2])
